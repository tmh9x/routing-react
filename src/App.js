import { Route, Routes } from "react-router-dom";

import About from "./views/About";
import Contact from "./views/Contact";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Home from "./views/Home";
import Login from "./views/Login";
import Main from "./components/Main";

function App() {
  return (
    <div className="flex flex-col h-screen">
      <Header />
      <Main>
        <Routes>
          <Route path="/" exact element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/login" element={<Login />} />
        </Routes>
      </Main>
      <Footer />
    </div>
  );
}

export default App;

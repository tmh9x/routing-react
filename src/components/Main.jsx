import React from "react";

const Main = ({ children }) => {
  return (
    <main className="flex-1" aria-label="Main content">
      {children}
    </main>
  );
};

export default Main;

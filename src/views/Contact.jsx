import React from "react";

const Contact = () => {
  return (
    <div className="container mx-auto">
      <h1 className="text-3xl font-bold mb-6" aria-label="Contact Page">
        Contact Page
      </h1>
      <div>
        <p className="text-lg text-gray-800">
          Please feel free to contact us if you have any questions or inquiries.
          You can reach us by email at contact@example.com or by phone at
          +123456789.
        </p>
      </div>
    </div>
  );
};

export default Contact;

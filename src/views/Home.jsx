import React from "react";

const sections = [
  {
    id: 1,
    title: "Section 1",
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.",
  },
  {
    id: 2,
    title: "Section 2",
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.",
  },
  {
    id: 3,
    title: "Section 3",
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.",
  },
  {
    id: 4,
    title: "Section 4",
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.",
  },
  {
    id: 5,
    title: "Section 5",
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.",
  },
  {
    id: 6,
    title: "Section 6",
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.",
  },
  {
    id: 7,
    title: "Section 7",
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.",
  },
  {
    id: 8,
    title: "Section 8",
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.",
  },
  {
    id: 9,
    title: "Section 9",
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.",
  },
  {
    id: 10,
    title: "Section 10",
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.",
  },
  {
    id: 11,
    title: "Section 11",
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.",
  },
  {
    id: 12,
    title: "Section 12",
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.",
  },
];

const Home = () => {
  return (
    <div className="container mx-auto">
      <h1 className="text-3xl font-bold mb-6">Home Page</h1>
      <ul>
        {sections.map((section) => (
          <div className="inline-block">
            <li
              key={section.id}
              className="w-48 border border-gray-300 p-2 m-2 rounded-lg shadow-lg hover:bg-gray-100 cursor-pointer"
              aria-label={`Section: ${section.title}`}
            >
              <h2 className="text-2xl font-semibold mb-2">{section.title}</h2>
              <p className="text-gray-700 text-xs w-36 h-12 line-clamp-3">
                {section.content}
              </p>
            </li>
          </div>
        ))}
      </ul>
    </div>
  );
};

export default Home;

import React from "react";

const About = () => {
  return (
    <div className="container mx-auto">
      <h1 className="text-3xl font-bold mb-6" aria-label="About Page">
        About Page
      </h1>
      <div className="flex items-center">
        <div className="bg-gray-100 p-6 rounded-lg shadow-md mr-4">
          <img
            src="https://via.placeholder.com/150"
            alt="Placeholder"
            className="w-32 h-32 object-cover rounded-full"
          />
        </div>
        <p className="text-lg text-gray-800">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed aliquet
          mauris nec elit bibendum, sit amet efficitur lectus tincidunt. Donec
          vitae neque et eros facilisis efficitur ac et magna. Nullam euismod
          magna non odio facilisis, vitae lobortis quam volutpat. Integer
          volutpat aliquet leo, sit amet varius libero tempor ut. Sed nec
          lacinia nulla, vel fermentum mauris. Nam vitae felis et arcu semper
          tincidunt. Morbi quis ullamcorper sapien, at rutrum neque. Aliquam nec
          convallis justo, at pharetra risus. Vivamus vel nisi luctus, vehicula
          arcu vel, faucibus ex. Mauris elementum nisl et massa lacinia, at
          dapibus velit lacinia. Aenean hendrerit, libero at auctor ultricies,
          orci nisl dignissim eros, id accumsan purus tortor sit amet nunc.
          Donec ut tempus nisl, nec pharetra nisi.
        </p>
      </div>
    </div>
  );
};

export default About;
